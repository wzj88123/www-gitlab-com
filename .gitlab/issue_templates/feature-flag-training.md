## Feature Flag Training

[Feature Flags](https://docs.gitlab.com/ee/development/feature_flags/) are used by GitLab engineers in order to roll out changes while 
monitoring the impact. They are **not the same** as the [**Operations > Feature Flag**](https://docs.gitlab.com/ee/user/project/operations/feature_flags.html) feature inside of GitLab. This template is intended to help understand 
how and when to use this tool in development.

### Steps

- [ ] Assign this issue to yourself with the title of Feature Flag Training - First Name Last Name - Q#YYYY
- [ ] [Get access](https://docs.gitlab.com/ee/development/feature_flags/controls.html#access) to control feature flags

### Review

- [ ] [Starting with Feature Flags in Development](https://docs.google.com/presentation/d/e/2PACX-1vTQM1dqVp2EWoIVOsKWv-Prbh3IVEWGsI0t-2rn0Rll-1B-UlqTn6DYFO0PaIjDLkRaWWVd0jEUwM1-/pub?start=false&loop=false&delayms=3000)

### Read

- [ ] [Including a feature behind a feature flag in the final release](https://docs.gitlab.com/ee/development/feature_flags/process.html#including-a-feature-behind-feature-flag-in-the-final-release)
- [ ] [Controlling feature flags](https://docs.gitlab.com/ee/development/feature_flags/controls.html)
- [ ] [Pete Hodgson & Martin Fowler on Feature Flags](https://martinfowler.com/articles/feature-toggles.html)
