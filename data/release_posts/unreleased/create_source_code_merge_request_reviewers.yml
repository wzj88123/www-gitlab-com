---
features:
  primary:
  - name: "Merge request reviewers"
    available_in: [core, starter, premium, ultimate]
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/user/project/merge_requests/getting_started#reviewer'
    image_url: '/images/unreleased/reviewers_sidebar.png'
    reporter: danielgruesso
    stage: create
    categories:
    - Code Review
    issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/216054'
    description: |
      Asking a colleague to review your code should be a routine part of contributing code, but it's often needlessly complex. A simple task like asking for a review can lead to confusion. For example, how should you ask? An email? comment? chat message? Without a formal process, reviews can be inconsistent and hard to keep track of. One option is to assign a reviewer to a merge request, but when both authors and reviewers appear in the same assignee field it makes it hard for other team members to know who's doing what. 

      GitLab 13.5 introduces merge request “reviewers,” which allow authors to request a review from someone. The new “reviewers” field allows users to be designated as reviewers in a similar way to assignees. The reviewers receive a notification inviting them to review the merge request. This provides a formal process for requesting a review and clarifies the roles of each user in a merge request.

      Future iterations will include showing the most relevant reviewers for a merge request as well as a streamlined merge request approval flow that puts reviewers at the center. You can follow along in the [merge request reviewer assignment epic](https://gitlab.com/groups/gitlab-org/-/epics/1823) for more details.
