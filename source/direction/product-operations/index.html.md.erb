---
layout: markdown_page
title: Product Direction - Product Operations
description: "Product Operations supports product management and cross-functional teams across GitLab"
canonical_path: "/direction/product-operations/"
---
- TOC
{:toc}

## Product Operations Overview

### What is Product Operations?

Product operations is an emerging function meant to support the scaling of product-led organizations with as little friction as possible. The role and focus of product operations can vary depending on business maturity, customer needs and the product itself. A **good** product operations function will define, communicate, and improve practices that can be standardized, such as tools, planning, team gatherings, and training. A **great** product operations function will sit at the intersection of product management, UX/design, and engineering to enable cross-functional teams to consistently, flexibly and creatively deliver outcomes  to customers/users via strong  feedback loops and process automation. An **excellent** product operations function will create and implement systems to continually drive partnership between product management and teams across the business, inclusive of marketing, sales and customer success, resulting in accelerated feedback loops, collection/exposure of critical data and improvement of feature adoption.

### What is Product Operations at GitLab?

At GitLab, Product Operations strives for the good, the great and the excellent! We are in a unique position to not only define and lead what this newly emerging function can do for software companies, but to  apply the strategies and tactics in the context of a highly technical, open core product with an all-remote, global team. This means that our definition of Product Operations won't just be ours. It'll be informed by millions of users, thousands of businesses, and hundreds of cultures, enabling us to build a truly diverse and inclusive product system that drives [efficiency](https://about.gitlab.com/handbook/values/#efficiency) and [results](https://about.gitlab.com/handbook/values/#results) without the traditional boundaries that restrain most product development methodologies. 

**Vision**:  A world class product system, that GitLab and all of GitLab’s customers and users can benefit from. 

**Mission**: Empower GitLab to be truly product-led and customer-centered, by iterating on Agile/Lean best practices with a remote-first mindset, one MR at a time.

#### Principles

- **We embody all [GitLab values](https://about.gitlab.com/handbook/values/), with [collaboration](https://about.gitlab.com/handbook/values/#collaboration) at the forefront.** Product Operations is only as good as the sum of its _contributions_ from the whole product development team and its stable counterparts. Teams that share accountability for (product) ownership invent better solutions and gain traction with less internal friction. These teams bond and rally around achieving a shared definition of success. 

- **We continually align around [our product principles](https://about.gitlab.com/handbook/product/product-principles/#our-product-principles), with a focus on understanding the customer problem rather than assuming we know the solution.** A single problem will have multiple solutions with varying impact potential. Customer-focused teams  maximize value by prioritizing one problem to generate multiple solutions, rather than assuming a singular solution to solve multiple problems.

- **We support product management to constantly sit at the intersection of Marketing, Sales, Customer Success, UX/Design and Engineering by building internal and external feedback loops.** The most important success metric for any (product) team is the time it takes to move through the Think-Make-Check (or Build-Measure-Learn) cycle. Teams that [iterate](https://about.gitlab.com/handbook/engineering/ux/how-we-work/#iteration) successfully and quickly through this cycle have continual access to qualitative and quantitative data through every phase of development and across every function in the business.

- **We grow high-functioning, cross-functional teams by creating flexible frameworks that support confident decision making along with the permission to fail, which in turn fuels velocity.** When teams have psychological safety, they are more likely to take risks and move faster, which ultimately leads to [bigger ideas](https://about.gitlab.com/handbook/engineering/ux/thinkbig/) and better [MVCs](https://about.gitlab.com/handbook/product/product-principles/#the-minimal-viable-change-mvc) toward those ideas. These teams lose judgment in favor of trust. 

- **We prioritize outcomes over outputs by eliminating waste in our a dual-track, ongoing [product development flow](https://about.gitlab.com/handbook/product-development-flow/).** The ultimate goal is delivering business impact to GitLab's customers and users. Anything that doesn’t contribute to this overarching goal is waste and will be removed from our workflows and processes.


#### Challenges 

**Finding the right balance of synchronous versus asynchronous collaboration as an all-remote team.** GitLab has a [unique culture ](https://about.gitlab.com/company/culture/all-remote/values/) of combined independence and ownership as a fully remote organization. We've carved the path for successful remote work with our [transparent](https://about.gitlab.com/company/culture/all-remote/values/#transparency) handbook first approach, which has also propelled a highly productive asynchronous team with an [everyone can contribute](https://about.gitlab.com/company/strategy/#contribute-with-gitlab) mindset. However, as GitLab has gone from fewer than 350 to over 1200 employees, we've seen some of our asynchronous processes fail serve us at scale.

At scale, leaning too much toward asynchronous results in opportunity gaps to collaborate on optimal outcomes within and across teams, and even operational chaos. Too much synchronous communication creates the overly burdened "red tape" operational inefficiencies that traditional companies suffer from across large teams and various time zones. So as we grow our team and our userbase, we have to continue adjusting the levers to establish the right convergence and divergence points, for product development teams and across the business.

**Creating and maintaining a psychologically safe space as we grow our headcount.** To date, GitLab has succeeded with a culture that is reasonably flat and [values](ttps://about.gitlab.com/company/culture/all-remote/values/) that promote level agnostic collaboration and contributions across the organization regardless of functional role. We've embraced a [bias toward action](https://about.gitlab.com/handbook/values/#bias-for-action), raising MRs to make micro improvements that can be quickly improved, revised or even removed, minimizing the fear of failure.

As we grow larger teams to support bigger enterprise customers, especially as an all remote team, it becomes more challenging to build human to human relationships within in and across teams to building trust and safety. And because every change we introduce into our system or into our product can have significant positive and negative impacts on thousands of our users, with real revenue ramification for larger customers, functional roles get more defined and the stakes become higher. This opens the door to hierarchy driven systems, competitively motivated behavior and fear based decision making, which don't promote risk-taking, voicing alternative opinions and creativity, which are the types of behavior that lead to innovative breakthroughs.

There is no one formula for this challenge, which all maturing organizations face. We'll have to continually lean into opportunities to build empathy amongst our teams by building frameworks to provide resources that foster positive emotions of curiosity, confidence, and inspiration.

**Providing access to actionable product usage data for GitLab product development teams** As GitLab's userbase grows, collecting quantitative data to identify patterns and trends while leveraging qualitative data to better understand the reason behind those patterns and trends will be key to delivering meaningful impact to customers. However, GitLab is committed to [user data privacy](https://about.gitlab.com/privacy/) and respects user-requested boundaries on data collection. Alternatives to product usage data, such as user surveys and testing can help fill some of the gaps but are not as scalable or unbiased. And while leveraging product usage data from SaaS users can yield meaningful insights for Self-managed users as well, it's not always an apples to apples comparison. We need to continually work on finding the balance between user privacy and providing the product teams access to the business intelligence they need to deliver meaningful impact to customers. Concurrently, we need to minimize the complexity and manual nature of effort needed by product development teams to access the most meaningful data available to them in the system by prioritizing instrumentation and providing standardized dashboards for teams to leverage and customize. 

#### Opportunities

Remote teams have the ability to  to move fast, in both parallel and non-parallel workflows, achieving optimal efficiency. 

As an open core product, what we build for ourselves at GitLab, can be productized for our customers and improved upon by our users. 

## Product Operations Areas of Focus

### Operationalizing Outcome Driven Products

##### Recently completed

[GitLab Official Milestone creation/close process](https://gitlab.com/gitlab-com/Product/-/issues/898)

[Automate Release Post to include new feature count](https://gitlab.com/gitlab-com/ceo-shadow/tasks/-/issues/108)

[Scaling the release post](https://docs.google.com/presentation/d/1_Osx3FrDxT4aqjl-Kc9QXgp30z0Pl1k4tBCv0DlZYkk/edit)

[Pilot: Draft release post blog content blocks in Issues and Epics](https://gitlab.com/gitlab-com/Product/-/issues/1392)

[Product Development Flow as a Framework](https://gitlab.com/groups/gitlab-com/-/epics/938)

13.5 Focus

##### Currently working on

[Improve and Consolidate Gitlab release notes](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8641)

[Unresolved/Unknown `release post item generator` errors](https://gitlab.com/gitlab-org/gitlab/-/issues/250030)

[Research: Drafting and reviewing release post item content blocks in feature issues rather than MRs](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8025)

[Product Development Flow as a Framework](https://gitlab.com/groups/gitlab-com/-/epics/938)

13.6 Focus, Preview Page

[Issue Prioritization Framework](https://gitlab.com/groups/gitlab-com/-/epics/928)

##### What's next

[Product Development Flow as a Framework](https://gitlab.com/groups/gitlab-com/-/epics/938)

13.7 Focus

[Automate Release Post Blog to include new feature count](https://gitlab.com/gitlab-com/ceo-shadow/tasks/-/issues/108)

Product Development Timeline: Sweep for alignment with upgraded Product Development Flow

### Building Qualitative & Quantitative Feedback Loops

##### Recently completed

[Gitlab PNPS: Q1 2020 Responder Outreach](https://gitlab.com/gitlab-com/Product/-/issues/984)

[PNPS Collection & Analysis - Q2 Email Surveys](https://gitlab.com/gitlab-com/Product/-/issues/1040)

[Post Purchase Survey Pilot](https://gitlab.com/gitlab-com/Product/-/issues/1404)

[Post Purchase Survey Pilot Analysis Q2](https://gitlab.com/gitlab-com/Product/-/issues/1265)

##### Currently working on

[Gitlab PNPS: Q2 2020 Responder Outreach](https://gitlab.com/gitlab-com/Product/-/issues/1430)

[PNPS Collection & Analysis - Q3 Email Survey - SaaS](https://gitlab.com/gitlab-com/Product/-/issues/1493)

[Post Purchase Survey Iteration 2](https://gitlab.com/gitlab-com/Product/-/issues/1403)

[Expose Tech Debt and UX Debt in DevOps stage and group dashboards](https://gitlab.com/gitlab-com/Product/-/issues/1409)

[Utilization data for our various release pages](https://gitlab.com/gitlab-com/Product/-/issues/835)

##### What's next

[Improve feedback loops between Product and Sales/Support/CS](https://gitlab.com/groups/gitlab-com/-/epics/398)

[Collaborate with TAMs to better understand post purchase product success/failure points](https://gitlab.com/gitlab-com/Product/-/issues/999)

Gitlab PNPS: Q3 2020 Responder Outreach

PNPS Collection - Q4 Email Survey - SaaS

### Scaling Product Knowledge

##### Recently completed

[Train EMs/PMs on iteration & integrate into onboarding](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/6713)

[Product Handbook: Structure content to better support how PMs do their job](https://gitlab.com/gitlab-com/Product/-/issues/857)

##### Currently working on

[How EMs, PMs, PDs and TWs collaborate to create feature documentation to the handbook](https://gitlab.com/gitlab-com/Product/-/issues/1300)

[Post Product Handbook Content Re-org Refinements](https://gitlab.com/gitlab-com/Product/-/issues/1293)

##### What's next

Product Handbook: Consolidate written content to minimze read times and clarify required knowledge

Product Handbook: Sweep content for opporutnities to visualize 

Product Handbook; Optimize with internal/external video references

Review and Update Product manager onboarding 

## Product Operations Resources

Links to PNPS slides

Links to PPS slides

