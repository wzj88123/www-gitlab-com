---
layout: handbook-page-toc
title: Support Leadership Sync
---

### Purpose

Support leadership (Managers, Senior Managers and Director) meet in cross-regional synchronous meetings weekly.

The purpose of these meetings is to:
- build relationships, trust and context between regional managers
- explain context for issues and explore creative ideas in real time
- ensure that each issue has a DRI for issues that do not yet have them
- refine the problem statement of each raised issue by clarifying intent

The purpose of these meetings is **not** to:
- figure out the priority of issues
- refine solutions or merge requests related to issues
- make decisions

**Note:** It is worth reiterating though that these meetings are not for making decisions or discussing things that could be discussed within issues.

### Organization of Support Leadership Meetings

1. The agenda and notes are in a Google Doc: [Support leadership calls agenda and notes](https://drive.google.com/drive/u/0/search?q=%22Support%20leadership%20calls%20agenda%20and%20notes%20%282020%29%22%20parent:1eBkN9gosfqNVSoRR9LkS2MHzVGjM5-t5) (internal only)
1. Most agenda items should be linked to an existing issue. If an item does not have an existing issue, raise one before putting it in the agenda.
1. Agenda items that do not need to be linked to an existing issue are:
   - Discussion surrounding a specific individual's promotion, performance or individual situation
   - Discussion about sensitive or confidential issues and processes
1. Every effort should be made to add agenda topics before the first meeting of
   the week, as this gives all managers an equal opportunity to participate in
   discussing the topics in at least one meeting. No topics should be added to
   the agenda between the start of the week’s second meeting and the end of the
   week’s last meeting.
1. Each meeting has a chairperson to ensure that voices are heard equally and we
   make progress through the agenda.
1. All participants should be familiar with the [Video Calls Section](/handbook/communication/#video-calls)
   of the Communication page in the Handbook.
1. See this video, [Managing Support Leadership Sync Agendas and Meetings](https://drive.google.com/drive/u/0/search?q=managing%20support%20leadership%20sync%20agendas%20and%20meetings%20parent:1h1iaTgBbZJZG7CBNYUCRQPhMXe1xXlbM),
for more information, including how to edit and use the agenda and notes document. 

### Call Procedure
#### As a chair
1. Review the meeting agenda and familiarize yourself with each item. 
1. Review the hiring data and be aware of anything that is of note for the regions that are meeting.
1. Review the upcoming support events. Be sure to mention anything that may have an action item, or may need an action item such as:
   1. *Group Conversation*: _Please take a look at the slides, and add any points of interest for the general company_
   1. *Holiday*: _Do we have (or need) a coverage plan for this holiday?_
   1. *Metrics Review*: _Is there anything that needs to be highlighted to the executive team?_ 
1. Be aware of when people unmute - this is an indication that they have something to say. If necessary, please interrupt and pass the
floor. We want everyone to contribute, so it's your job to make sure that this can happen.
1. Assign any action items - including summarizing the discussion in an issue.

#### As a manager
* Before the meeting: 
    * add agenda items no later than the start of the week's second meeting
    * review each agenda item. If you have any feedback, don't hold it for the meeting but comment directly on the issue.
* During the meeting:
    * be brief: everyone should be familiar with the discussion
    * be clear: why did you bring this item to this meeting?
    * be polite: avoid interrupting when possible (For more on this see point 13 in the [Video Calls Section](/handbook/communication/#video-calls) of the Communication page)
* After the meeting: 
   * note any points that you made during the meeting that were relevant to the discussion.
