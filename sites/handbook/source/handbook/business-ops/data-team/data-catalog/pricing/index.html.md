---
layout: handbook-page-toc
title: "Pricing Analysis"
---

## On this page
{:.no_toc}

- TOC
{:toc}

---

## Pricing Analysis

Pricing is the process of analyzing the value customers receive from GitLab at specific price points along with the profitability of those price points. The analysis also includes understanding how these prices affect the overall business and determining what the optimal price points are for customers and GitLab's profitability. The Pricing Analysis page will provide the information and tools that GitLab team members can use to explore our current pricing strategy and develop insights to optimize it.

This data solution delivers three [Self-Service Data](/handbook/business-ops/data-team/direction/self-service/) capabilities:

1. Dashboard Viewer - a series of new SiSense dashboards to visualize customers discounts, renewals, customers counts by different segments and more. (Self-Service Dashboard)
1. Dashboard Developer - a new SiSense data model containing the complete dimensional model components to build new dashboards and link existing dashboards to pricing analysis data.
1. SQL Developer - an Enterprise Dimensional Model subject area

From a Data Platform perspective, the solution delivers:

1. An extension to the Enterprise Dimensional Model for Pricing Analysis
1. Testing and data validation extensions to the Data Pipeline Health dashboard
1. ERDs, dbt models, and related platform components

### Data Security Classification

`Coming soon`

### Key Terms

`Coming soon`

## Self-Service Data Solution

### Self-Service Dashboard Viewer

`Coming soon`

### Self-Service Dashboard Developer

`Coming soon`

### Self-Service SQL Developer

#### Key Fields and Business Logic

`Coming soon`

#### Entity Relationship Diagrams

`Coming soon`

#### Reference SQL

`Coming soon`

## Data Platform Solution

### Data Lineage

`Coming soon`

### DBT Solution

`Coming soon`

## Trusted Data Solution

See overview at [Trusted Data Framework](https://about.gitlab.com/handbook/business-ops/data-team/direction/trusted-data/)

[dbt guide examples](https://about.gitlab.com/handbook/business-ops/data-team/platform/dbt-guide/#trusted-data-framework) for
details and examples on implementing further tests

### Dashboards 

* [PnP Analysis, Test and Research](https://app.periscopedata.com/app/gitlab/735150/PnP-Analysis,-Test-and-Research)
* [(WIP) Pricing Customer Overview Dashboard](https://app.periscopedata.com/app/gitlab/748119/DRAFT---Pricing-EoA-Customer-Overview) 

### EDM Enterprise Dimensional Model Validations

`Coming Soon`

### RAW Source Data Pipeline validations

[Data Pipeline Health Validations](https://app.periscopedata.com/app/gitlab/715938/Data-Pipeline-Health-Dashboard)
