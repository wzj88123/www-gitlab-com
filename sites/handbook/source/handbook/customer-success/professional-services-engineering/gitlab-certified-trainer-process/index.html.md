---
layout: handbook-page-toc
title: "GitLab Certified Trainer - Candidate Process"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## GitLab Certified Trainer - Candidate Process

Professional Services now has a certification process for trainers to validate their readiness to deliver Education Services offerings. Here are the steps required to earn the certification for the course you want to deliver. 


1. [Create a new issue](https://gitlab.com/gitlab-com/customer-success/professional-services-group/education-services/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=#) in the Education Services namespace using the certified-trainer_candidate issue template.
1. Complete each of the tasks listed in the Candidate Tasks section of the issue description.
1. Reach out to the Project Coordinator to coordinate  your schedule for shadowing and evaluation activities.
1. Request feedback and advice from your Evaluator as needed.
1. Once you are certified, add the certification to your LinkedIn profile!
